
# DevOps django Sample app


This is being used as an example app for a Introduction to DevOps  tutorial @<>

#  Django Photo Gallery Sample #

![Django Photo Gallery Sample Version](https://img.shields.io/badge/Version-0.0.2-green.svg)

## Summary

This sample contains a Django 2.0.x Image Gallery Site. The album images are optimized for performance. The Django Photo Gallery Sample is responsive and mobile/device friendly.

![Django Photo Gallery Sample](./assets/django_photo_gallery_explore.gif)

### Create an album from the Django admin panel


## Tested with Django / Python
![Python](https://img.shields.io/badge/Python-2.3.6-green.svg)
![Django](https://img.shields.io/badge/Django-2.0.4-green.svg)

## Solution

Solution|Author(s)
--------|---------
Django Photo Gallery | Velin Georgiev ([@VelinGeorgiev](https://twitter.com/velingeorgiev))



## Disclaimer
**THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.**



## Sample data cleanup
To cleanup the sample data delete the sql lite database and the media folder files. Create new database and run the sample again.

## No validation on the form
This is sample. I decided to keep it simple and let the validation to be added by you.

## Sharing is Caring

Star if you like it :)
